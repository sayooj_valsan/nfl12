package com.phunware.nfl12.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.phunware.nfl12.DataHelper;
import com.phunware.nfl12.R;
import com.phunware.nfl12.model.Venue;

public class VenueAdapter extends BaseAdapter {

	Context context;

	public VenueAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		return DataHelper.getIntance().getAllVenues().size();
	}

	@Override
	public Venue getItem(int position) {
		return DataHelper.getIntance().getAllVenues().get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Using ViewHolder Pattern
		ViewHolder viewHolder;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.rowlayout, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.name = (TextView) convertView
					.findViewById(R.id.venueName);
			viewHolder.address = (TextView) convertView
					.findViewById(R.id.venueAddress);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();

		}

		viewHolder.name.setText(getItem(position).getName());
		viewHolder.address.setText(getItem(position).getAddress());

		return convertView;
	}

	static class ViewHolder {
		TextView name;
		TextView address;
	}

}
