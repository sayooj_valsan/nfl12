package com.phunware.nfl12.network;

import java.util.ArrayList;

import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.phunware.nfl12.DataHelper;
import com.phunware.nfl12.interfaces.ParseCompleteListener;
import com.phunware.nfl12.model.Venue;

/**
 * VolleyHelper takes care of network request.
 * @author Sayooj Valsan
 *
 */
public class VolleyHelper {
	private String TAG = VolleyHelper.class.getSimpleName();

	RequestQueue queue;
	ParseCompleteListener listener;
	String url;
	public VolleyHelper(final String url, RequestQueue queue) {

		this.url = url;
		this.queue = queue;
	}
	
	public void execute(final ParseCompleteListener listener)
	{
		this.listener = listener;

		//Add to request Queue
		//Volley will internally creates thread for network request, so we dont have to worry about background thread here
		StringRequest jsObjRequest = new StringRequest(url, new Listener<String>() {

			@Override
			public void onResponse(final String response) {
				
				
				//Start a new Thread to take care of parsing. Since our data is huge it makes sense to do this in a background thread
				Thread d  = new Thread(new Runnable() {
					

					@Override
					public void run() {
						//Parse the data
				        Gson gson =
				        new GsonBuilder()
				        .setDateFormat("yyyy-MM-dd HH:mm:ss Z").create();
				        ArrayList<Venue> venues = gson.fromJson(response, new TypeToken<ArrayList<Venue>>(){}.getType());
				        DataHelper.getIntance().addAllVenues(venues);
						Log.d(TAG, "Done parsing.. data, size = " + venues.size());
						
						if(VolleyHelper.this.listener != null){
							
							VolleyHelper.this.listener.onParseCompleteListener(true);
						}
					}
				});
				d.start();
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {

				Log.e(TAG, error.getMessage());
				if(VolleyHelper.this.listener !=null)
				{
					VolleyHelper.this.listener.onParseCompleteListener(false);
				}
			}
		});
		
		queue.add(jsObjRequest);
	}
	
	public void removeListener()
	{
		this.listener = null;
	}
}
