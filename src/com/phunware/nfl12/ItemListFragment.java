package com.phunware.nfl12;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.phunware.nfl12.adapter.VenueAdapter;
import com.phunware.nfl12.interfaces.ParseCompleteListener;
import com.phunware.nfl12.network.VolleyHelper;
import com.phunware.nfl12.utils.Utils;

/**
 * A list fragment representing a list of Items. This fragment also supports
 * tablet devices by allowing list items to be given an 'activated' state upon
 * selection. This helps indicate which item is currently being viewed in a
 * {@link ItemDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */ 
public class ItemListFragment extends ListFragment  implements ParseCompleteListener{

	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */
	private static final String STATE_ACTIVATED_POSITION = "activated_position";

	/**
	 * The fragment's current callback object, which is notified of list item
	 * clicks.
	 */
	private Callbacks mCallbacks = sDummyCallbacks;

	/**
	 * The current activated item position. Only used on tablets.
	 */
	private int mActivatedPosition = ListView.INVALID_POSITION;

	private VenueAdapter adapter;

	private VolleyHelper helper;

	/**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface Callbacks {
		/**
		 * Callback for when an item has been selected.
		 */
		public void onItemSelected(String id);
	}

	/**
	 * A dummy implementation of the {@link Callbacks} interface that does
	 * nothing. Used only when this fragment is not attached to an activity.
	 */
	private static Callbacks sDummyCallbacks = new Callbacks() {
		@Override
		public void onItemSelected(String id) {
		}
	};

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ItemListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Init Adapter
		if(adapter == null)
		{
			adapter = new VenueAdapter(getActivity().getApplicationContext());
			setListAdapter(adapter);
		}
		//Kick off to fetch Data
		if(Utils.isNetworkAvailable(getActivity().getApplicationContext()))
		{
			fetchData();
		}
		else
		{
			//Show no internet dialog
			showNoInternetDialog();
		}

	}

	/**
	 * Fetch data from internet
	 */
	private void fetchData() {
		if(helper == null){
			helper = new VolleyHelper(Constants.SOURCE_URL, ((NFLApplication)getActivity().getApplication()).getVolleyRequestQueue());
		}
		helper.execute(this);
	}

	
	
	/**
	 * Show Alert dialog showing no internet connection messages
	 */
	private void showNoInternetDialog() {

		
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.no_internet_head);
        alertDialogBuilder.setMessage(R.string.no_internet_desc);
        //null should be your on click listener
        alertDialogBuilder.setPositiveButton(R.string.yes, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(Utils.isNetworkAvailable(getActivity().getApplicationContext())){
					fetchData();
				}
				else{
					showNoInternetDialog();
				}
				
			}
		});
        alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().finish();
            }
        });
        alertDialogBuilder.create().show();


	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// Restore the previously serialized activated item position.
		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
		
		//Show empty text
		setEmptyText(getResources().getString(R.string.loading));

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = sDummyCallbacks;
		
		if(helper != null){
			helper.removeListener();
			helper = null;
		}
			
			
		
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);

		// Notify the active callbacks interface (the activity, if the
		// fragment is attached to one) that an item has been selected.
		
		
		mCallbacks.onItemSelected(String.valueOf(DataHelper.getIntance().getAllVenues().get(position).getId()));
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	public void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically
		// give items the 'activated' state when touched.
		getListView().setChoiceMode(
				activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
						: ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}
	
	@Override
	public void onParseCompleteListener(final boolean isSuccess) {
		if(isDetached())
			return;
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				if(isSuccess){
					adapter.notifyDataSetChanged();
				}
				else{
					showErrorMessage();
				}
				
			}

			
		});
	}
	
	/**
	 * We had some issues with network parsing / server down etc. Let the user know about it
	 */
	private void showErrorMessage() {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.error_head);
        alertDialogBuilder.setMessage(R.string.error_desc);
        //null should be your on click listener
        alertDialogBuilder.setPositiveButton(R.string.ok, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().finish();

				
			}
		});
      
        alertDialogBuilder.create().show();
		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		getListView().setSelector(R.drawable.selector);
		super.onActivityCreated(savedInstanceState);
	}
}
