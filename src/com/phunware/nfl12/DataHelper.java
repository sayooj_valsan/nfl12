package com.phunware.nfl12;

import java.util.ArrayList;

import com.phunware.nfl12.model.Venue;


/**
 * Singleton DataHelper. Takes care of storing data
 * @author Sayooj Valsan
 *
 */
public class DataHelper {


	private static  DataHelper INSTANCE = new DataHelper();
	private static ArrayList<Venue> venues = new ArrayList<Venue>();

	public static DataHelper getIntance(){
		return INSTANCE;
	}

	//Enforce Singleton
	private DataHelper(){

	}

	public void addVenue(Venue venue){
		venues.add(venue);
	}


	public void addAllVenues(ArrayList<Venue> venue){
		//Clear old values
		venues.clear();
		venues.addAll(venue);
	}

	//Given an Id, find a venue
	public Venue findVenue(long id){

		Venue tempVenue = new Venue();
		tempVenue.setId(id);
		if(venues.contains(tempVenue)){
			//We got te venue we are looking for
			int position = venues.indexOf(tempVenue);
			return venues.get(position);
		}
		return null;
	}

	public void clearAll(){
		venues.clear();
	}

	public ArrayList<Venue> getAllVenues(){
		return venues;
	}

}
