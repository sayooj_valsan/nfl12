package com.phunware.nfl12;

import java.text.SimpleDateFormat;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.phunware.nfl12.model.ScheduleItem;
import com.phunware.nfl12.model.Venue;

/**
 * Detailed Fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link ItemListActivity} in two-pane mode (on tablets) or a
 * {@link ItemDetailActivity} on handsets.
 */


public class ItemDetailFragment extends Fragment {
	
	static SimpleDateFormat simpleDateformatPart1 = new SimpleDateFormat("EEEE M/dd h:mm a"); // the day of the week abbreviated
	static SimpleDateFormat simpleDateformatPart2 = new SimpleDateFormat("h:mm a"); // the day of the week abbreviated

	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private Venue mItem;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ItemDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			
			long id = Long.parseLong(getArguments().getString(
					ARG_ITEM_ID));
			
			mItem = DataHelper.getIntance().findVenue(id);
//			mItem = ScheduleItem.ITEM_MAP.get(getArguments().getString(
//					ARG_ITEM_ID));
		}
		setHasOptionsMenu(true);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_item_detail,
				container, false);
		
		if (mItem != null) {
			
			//Set the Venue name
			((TextView) rootView.findViewById(R.id.textViewName))
					.setText(mItem.getName());
			
			//Set the Address part 1
			((TextView) rootView.findViewById(R.id.textViewAddress))
			.setText(mItem.getAddress());

			//Set the the other Address info
			StringBuffer addressString = new StringBuffer().append(mItem.getCity()).append(", ").append(mItem.getState()).append(" ").append(mItem.getZip());
			((TextView) rootView.findViewById(R.id.textViewAddress2))
			.setText(addressString.toString());


			//Lets check if we have some scehdule
			List<ScheduleItem> schedule = mItem.getSchedule();

			if(schedule !=null && !schedule.isEmpty()){
				
				StringBuffer scheduleString = new StringBuffer();
				//For every schedule
				for(ScheduleItem item: schedule){
					String date1 = simpleDateformatPart1.format(item.getStartDate());
					String date2 = simpleDateformatPart2.format(item.getEndDate());
					scheduleString.append(date1+ " to " +  date2).append("\n");
				}
				//Finally show everything on the textview
				((TextView) rootView.findViewById(R.id.textViewSchedule)).setText(scheduleString.toString());

			}
			
			//Lets use volley NetworkImageView
			NetworkImageView imageView = ((NetworkImageView) rootView.findViewById(R.id.imageView));
			//If we dont have any image
			if(mItem.getImageUrl() == null || mItem.getImageUrl().length() == 0){
				imageView.setDefaultImageResId(R.drawable.noimage);
			}
			else{
				//Lets put a placeholder
				imageView.setDefaultImageResId(R.drawable.imageloading);
				imageView.setImageUrl(mItem.getImageUrl(), ((NFLApplication)getActivity().getApplication()).getImageLoader());
			}
		}

		return rootView;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//If user is sharing
		if(item.getItemId() == R.id.shareitem){
			share();
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	/**
	 * Share information across
	 */
	private void share() {

		//get the current item
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_SUBJECT, mItem.getName());
		String address = mItem.getAddress() + "\n" + mItem.getCity() + ", " + mItem.getState() + " " + mItem.getZip();
		sendIntent.setType("text/plain");
		sendIntent.putExtra(Intent.EXTRA_TEXT, address);

		startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menuitem, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

}
