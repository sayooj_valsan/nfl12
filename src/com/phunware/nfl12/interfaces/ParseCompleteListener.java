package com.phunware.nfl12.interfaces;

public interface ParseCompleteListener {
	
	/**
	 * onParseCompleteListener lets us know if parsing was success or not
	 * @param success if we successully parsed the data.
	 */
	public void onParseCompleteListener(boolean success);

}
